import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sanitation/pages/search.dart';
import 'package:sanitation/pages/splash_page.dart';
import 'package:sanitation/pages/start_page.dart';

import 'pages/places_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.light,
       ));
         SystemChrome.setPreferredOrientations(
          [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: '',
     // theme: ThemeData(fontFamily: 'DancingScript'),
      home: StartPage(),
    );
  }
}
