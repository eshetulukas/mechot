import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import '../models/place_model.dart';
import 'dart:async';
import 'dart:convert';

class LocationService {
  static var name;
  Set<String> names = {'cafe', 'bar', 'hotel'};
  static final _locationService = new LocationService();
  static Future<LocationData> getLocation() async {
    var location = await Location().getLocation();
    return location;
  }

  static LocationService get(var nameOfPlace) {
    name = nameOfPlace;
    return _locationService;
  }


  Future<List<PlaceDetail>> getNearbyPlaces() async {
    var mylocation = await getLocation();
    double lat = mylocation.latitude;
    double long = mylocation.longitude;
    print(mylocation.latitude);
    print(mylocation.longitude);
    print(lat.toString());


    final String url =
        "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=1000&types=$name&key=AIzaSyBjhUtgwOb54kM9QIkoitzzVhhG7rSU9vQ";
     var reponse = await http.get(url, headers: {"Accept": "application/json"});
      List dataCafe = json.decode(reponse.body)["results"];
      print(dataCafe);
      var places = <PlaceDetail>[];
      dataCafe.forEach((f) => places.add(new PlaceDetail(f["place_id"], f["name"],
        f["icon"], f["rating"], f["vicinity"], lat, long,f["geometry"]["location"]["lat"],f["geometry"]["location"]["lng"])));  
    return places;
  }
  
  ///reviews.map((f)=> new Review.fromMap(f)).toList()

}