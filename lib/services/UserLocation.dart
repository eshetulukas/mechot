import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:sanitation/models/place_detail_model.dart';
import '../models/place_model.dart';
import 'dart:async';
import 'dart:convert';

class UserLocation {
  static final _locationService = new UserLocation();
  static Future<LocationData> getLocation() async {
    var location = await Location().getLocation();
    return location;
  }

  static UserLocation get() {
    return _locationService;
  }
  Future<List<PlaceDetailModel>> getNearbyPlaces() async {
    var mylocation = await getLocation(
      
    );
   

    double lat = mylocation.latitude;
    double long = mylocation.longitude;
    final String url =
      "http://35.246.213.39:81/api/toilet/nearby/all?lat=$lat&lng=$long&radius=400";
    var reponse = await http.get(url, headers: {"Accept": "application/json"});
    if (reponse.statusCode == 200) {

      // var counter =
      //     await http.get('http://35.246.213.39:81/api/toilet/visit/toilet_id');
      List placeList = json.decode(reponse.body)['message'];
      print(placeList);
      print("helooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo");

      var places = <PlaceDetailModel>[];
      placeList.forEach((f) {
        double distance = 0;
        if(f["distance"]*1000>200 &&f["distance"]*1000<1000)
        {
          distance = f["distance"]*1.22;
        }
        else
        if(f["distance"]*1000>1000)
        {
           distance = f["distance"]*1.5;
        }
        places.add(new PlaceDetailModel(
            f["name"],
            f["address"],
            f["lat"],
            f["lng"],
            f["female_friendly"],
            f["rating"],
            lat,
            long,
            distance,
            f["id"],
            f["male"],
            f["female"],
            f["disable_access"]
            ));
      });
      return places;
    }
  }
}
