import 'package:flutter/material.dart';
// import 'package:liquid_swipe/Constants/Helpers.dart';
import 'package:liquid_swipe/liquid_swipe.dart';
// import 'package:liquid_swipe/page.dart';
import 'package:sanitation/pages/search.dart';
import 'package:sanitation/pages/start_page.dart';

class Splash extends StatelessWidget {
  Splash({Key key}) : super(key: key);
  static BuildContext contexts;
  List<Container> pagesSwipe(BuildContext context) {
    return [
      Container(
        color: Color(0xFFff0066),
        height: MediaQuery.of(context).size.height,
        child: Stack(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  alignment: Alignment.center,
                  height: MediaQuery.of(context).size.height,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(500),
                        bottomRight: Radius.circular(500)),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 110.0),
                        child: Image.asset("assets/service1.png", fit: BoxFit.contain),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(40.0),
                        child: Text(
                          'Some 842 000 people in low- and middle-income countries die as a result of inadequate water, sanitation and hygiene each year, representing 58% of total diarrhoeal deaths. Poor sanitation is believed to be the main cause in some 280 000 of these deaths.',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.lens,
                        color: Colors.white,
                        size: 15,
                      ),
                      SizedBox(width: 5),
                      Icon(
                        Icons.radio_button_unchecked,
                        color: Colors.black,
                        size: 15,
                      ),
                      SizedBox(width: 5),
                      Icon(
                        Icons.radio_button_unchecked,
                        size: 15,
                      ),
                      SizedBox(width: 5),
                    ],
                  ),
                ),
              ),
            ]),
      ),
      Container(
        color: Colors.purple,
        child: Container(
          child: Stack(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
               Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  alignment: Alignment.center,
                  height: MediaQuery.of(context).size.height,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(500),
                        bottomRight: Radius.circular(500)),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 110.0),
                        child: Image.asset("assets/service1.png", fit: BoxFit.contain),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(40.0),
                        child: Text(
                          'Some 842 000 people in low- and middle-income countries die as a result of inadequate water, sanitation and hygiene each year, representing 58% of total diarrhoeal deaths. Poor sanitation is believed to be the main cause in some 280 000 of these deaths.',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.radio_button_unchecked,
                          size: 15,
                        ),
                        SizedBox(width: 5),
                        Icon(
                          Icons.lens,
                          color: Colors.white,
                          size: 15,
                        ),
                        SizedBox(width: 5),
                        Icon(
                          Icons.radio_button_unchecked,
                          size: 15,
                        ),
                        SizedBox(width: 5),
                      ],
                    ),
                  ),
                )
              ]),
        ),
      ),
      Container(
        color: Color(0xFF4E586E),
        child: GestureDetector(
          onTap: () {
            Navigator.of(context)
                .push(new MaterialPageRoute(builder: (BuildContext context) {
              return StartPage();
            }));
          },
           onTapDown: (TapDownDetails details) {
                 Navigator.of(context)
                .push(new MaterialPageRoute(builder: (BuildContext context) {
              return Search();
            }));
              },
              onHorizontalDragDown: (DragDownDetails details){ Navigator.of(context)
                .push(new MaterialPageRoute(builder: (BuildContext context) {
              return Search();
            }));},
          child: Container(
            child: Stack(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                 Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  alignment: Alignment.center,
                  height: MediaQuery.of(context).size.height,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(500),
                        bottomRight: Radius.circular(500)),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 110.0),
                        child: Image.asset("assets/service1.png", fit: BoxFit.contain),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(40.0),
                        child: Text(
                          'Some 842 000 people in low- and middle-income countries die as a result of inadequate water, sanitation and hygiene each year, representing 58% of total diarrhoeal deaths. Poor sanitation is believed to be the main cause in some 280 000 of these deaths.',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.radio_button_unchecked,
                            color: Colors.black,
                            size: 15,
                          ),
                          SizedBox(width: 5),
                          Icon(
                            Icons.radio_button_unchecked,
                            size: 15,
                            color: Colors.black,
                          ),
                          SizedBox(width: 5),
                          Icon(
                            Icons.lens,
                            color: Colors.white,
                            size: 15,
                          ),
                          SizedBox(width: 5),
                        ],
                      ),
                    ),
                  )
                ]),
          ),
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    //contexts = context;
    return Scaffold(
      body: LiquidSwipe(
        pages: pagesSwipe(context),
        enableLoop: true,
        fullTransitionValue: 500,
        enableSlideIcon: true,
        waveType: WaveType.liquidReveal,
        positionSlideIcon: 0.5,
      ),
    );
  }
}
