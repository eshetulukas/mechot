import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:sanitation/models/place_detail_model.dart';
import 'package:sanitation/models/place_model.dart';
import 'package:sanitation/services/UserLocation.dart';
import 'package:url_launcher/url_launcher.dart';

class Direction extends StatefulWidget {
  double lat;
  double long;
  var _places;
  var points;
  var distance;
  var location_lat;
  var location_long;
  Direction(this.lat, this.long, this.distance, this.location_lat,
      this.location_long, this._places) {
    points = LatLng(lat, long);
  }

  @override
  _DirectionState createState() => _DirectionState();
}

class _DirectionState extends State<Direction> {
  void _launchMapsUrl(var lat, var long) async {
    final url =
        'https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination=$lat,$long';

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Set<Polyline> _polylines = {};
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  GoogleMapController mapController;
  LatLng currentLocation;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  StreamController<LatLng> locationController =
      StreamController<LatLng>.broadcast();
  Stream<LatLng> get locationStream => locationController.stream;

  Set<Marker> createMarker() {
    currentLocation = widget.points;
    var allMarkers = Set<Marker>();
    allMarkers.add(
      Marker(
          markerId: MarkerId("start"),
          position: widget.points,
          icon:
              BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure),
          infoWindow: InfoWindow(title: "My location")),
    );

    if (widget._places != null) {
      allMarkers.add(
        Marker(
            markerId: MarkerId("end"),
            position: LatLng(
                widget._places.locaiton_lat, widget._places.location_long),
            infoWindow: InfoWindow(title: widget._places.name.toString())),
      );
    }

    return allMarkers;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //  UserLocation.get().getPlace().then((data) {
    //     //getData();
    //     this.setState(() {

    //     });
    //   });
    // getData();
    initMarker();
    //MapsLauncher.launchQuery("https://www.google.com/maps/dir/icong/airport");

    // MapsLauncher.launchCoordinates(
    //     widget._places.locaiton_lat, widget._places.location_long);
    // MapsLauncher.createQueryUrl(query)
  }

  createAlert(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              backgroundColor: Colors.white10,
              title: Container(
                //color: Colors.black,
                child: Center(
                    child: Text(
                  'Thank you for rating   ${widget._places.name} ',
                  maxLines: 1,
                  softWrap: true,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 13.5,
                      color: Colors.white,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.bold),
                )),
              ));
        });
  }

  sendData() {
    int rating = rate1 + rate2 + rate3 + rate4 + rate5;
    
    print(widget._places.id.toString());
     print(rating.toString());
    //print("dddddddddddddddddddddddddddddddddddddddddddddddddddddddduuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuud");
    http.put("http://35.246.213.39:81/api/toilet/rate/${widget._places.id}",
            body: {
          "rating": "$rating",
        }).
        // http.post("https://newdayshotel.com/demo/get.php",body:{
        //     "name": "g$rating",
        //   }).
        then(
      (value) {
        if (value.statusCode == 200) {
          print(value.body.toString());
        //  Navigator.of(context).pop(false);
          print(
              "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
        } else {
          print(value.statusCode.toString());
        }
      },
    );
  }

  List ratingValue = new List();
  bool isTouched = false;
  // setStart(int index) {
  //   if (ratingValue != null) {
  //    if (0 == ratingValue[index-1]) return false;
  //    else return true;
  //   }
  // }
  int rate1 = 0;
  int rate2 = 0;

  int rate3 = 0;

  int rate4 = 0;

  int rate5 = 0;

  rateToilet(int index) {
    setState(() {
      if (0 == ratingValue[index - 1]) {
        ratingValue[index - 1] = 1;
        print(ratingValue.toString());
      } else {
        ratingValue[index - 1] = 1;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    for (int i = 1; i <= 5; i++) {
      ratingValue.add(0);
    }
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              padding: EdgeInsets.all(0),
              child: GoogleMap(
                mapType: MapType.normal,
                myLocationEnabled: true,
                compassEnabled: true,
                markers: createMarker(),
                initialCameraPosition: CameraPosition(
                    target: LatLng(widget.lat, widget.long), zoom: 15),
              ),
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.36,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                ),
                child: Column(children: <Widget>[
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(0),
                        color: Colors.pink,
                      ),
                      height: 40,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black54,
                              offset: Offset(0.0, .0),
                              blurRadius: 0.0,
                            ),
                          ],
                        ),
                        height: 40,
                        width: MediaQuery.of(context).size.width * 1,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: Center(
                            child: Row(
                              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.33,
                                  padding: EdgeInsets.only(top: 6),
                                  child: Text(
                                    "RATE THIS TOILET",
                                    style: TextStyle(
                                        fontSize: 13.0,
                                        color: Colors.white,
                                        fontFamily: "Raleway",
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Container(
                                  //  color: Colors.white,
                                  width: 0,
                                  alignment: Alignment.topCenter,
                                ),
                                Expanded(
                                  child: Row(children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        //  color: Colors.white,
                                        width: 30,
                                        alignment: Alignment.topCenter,
                                        child: IconButton(
                                            icon: Icon(
                                              rate1 == 0
                                                  ? Icons.star_border
                                                  : Icons.star,
                                              color: Colors.yellow,
                                              size: 24,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                if (rate1 == 0)
                                                  rate1 = 1;
                                                else
                                                  rate1 = 0;
                                              });
                                            }),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        //  color: Colors.white,
                                        width: 30,
                                        alignment: Alignment.topCenter,
                                        child: IconButton(
                                            icon: Icon(
                                              rate2 == 0
                                                  ? Icons.star_border
                                                  : Icons.star,
                                              color: Colors.yellow,
                                              size: 24,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                if (rate2 == 0)
                                                  rate2 = 1;
                                                else
                                                  rate2 = 0;
                                              });
                                            }),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        //  color: Colors.white,
                                        width: 30,
                                        alignment: Alignment.topCenter,
                                        child: IconButton(
                                            icon: Icon(
                                              rate3 == 0
                                                  ? Icons.star_border
                                                  : Icons.star,
                                              color: Colors.yellow,
                                              size: 24,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                if (rate3 == 0)
                                                  rate3 = 1;
                                                else
                                                  rate3 = 0;
                                              });
                                            }),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        //  color: Colors.white,
                                        width: 30,
                                        alignment: Alignment.topCenter,
                                        child: IconButton(
                                            icon: Icon(
                                              rate4 == 0
                                                  ? Icons.star_border
                                                  : Icons.star,
                                              color: Colors.yellow,
                                              size: 24,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                if (rate4 == 0)
                                                  rate4 = 1;
                                                else
                                                  rate4 = 0;
                                              });
                                            }),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        //  color: Colors.white,
                                        width: 30,
                                        alignment: Alignment.topCenter,
                                        child: IconButton(
                                            icon: Icon(
                                              rate5 == 0
                                                  ? Icons.star_border
                                                  : Icons.star,
                                              color: Colors.yellow,
                                              size: 24,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                if (rate5 == 0)
                                                  rate5 = 1;
                                                else
                                                  rate5 = 0;
                                              });
                                            }),
                                      ),
                                    ),
                                    Container(
                                      //  color: Colors.white,
                                      width: 5,
                                      alignment: Alignment.topCenter,
                                    ),
                                  ]),
                                ),
                                SizedBox(width: 7),
                                GestureDetector(
                                    onTap: () {
                                      createAlert(context);
                                      sendData();
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: Container(
                                        width: 75,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors.white, width: 2),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10.0)),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: Text(
                                            "Submit",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontFamily: "Raleway"),
                                          ),
                                        ),
                                      ),
                                    ))
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    decoration: BoxDecoration(
                      color: Colors.pink,
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black38,
                            offset: Offset(0.0, 0.0),
                            blurRadius: .0,
                          ),
                        ],
                      ),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Row(
                              children: <Widget>[
                                Flexible(
                                  flex: 2,
                                  child: Container(
                                    //width: MediaQuery.of(context).size.width * ,
                                    // color: Colors.white38,
                                    child: Stack(
                                      children: [
                                        Center(
                                          child: Container(
                                            margin: EdgeInsets.symmetric(
                                              horizontal: 10.0,
                                              vertical: 20.0,
                                            ),
                                            decoration: BoxDecoration(
                                              // color: Colors.black,
                                              borderRadius:
                                                  BorderRadius.circular(4.0),
                                            ),
                                            child: Stack(
                                              children: <Widget>[
                                                Row(
                                                  children: [
                                                    SizedBox(width: 8.0),
                                                    Expanded(
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Container(
                                                            child: Text(
                                                              //_places[index].name,
                                                              widget
                                                                  ._places.name,
                                                              // maxLines: 1,
                                                              // softWrap: true,
                                                              // overflow:
                                                              //     TextOverflow
                                                              //         .ellipsis,
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      13.5,
                                                                  color: Colors
                                                                      .white,
                                                                  fontFamily:
                                                                      "Raleway",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ),
                                                          SizedBox(height: 8.0),
                                                          Text(
                                                            widget._places
                                                                .address,
                                                            maxLines: 1,
                                                            softWrap: true,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            style: TextStyle(
                                                                fontSize: 14.0,
                                                                color: Colors
                                                                    .white,
                                                                fontFamily:
                                                                    "Raleway",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w300),
                                                          ),
                                                          SizedBox(height: 8.0),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Container(
                                                                // width: 170.0,
                                                                child: Text(
                                                                  widget._places
                                                                              .rating !=
                                                                          null
                                                                      ? "Rating  ${widget._places.rating}"
                                                                      : "Rating ",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      fontFamily:
                                                                          "Raleway",
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w300),
                                                                ),
                                                              ),
                                                              Icon(
                                                                Icons.star,
                                                                color: Colors
                                                                    .yellow,
                                                              ),
                                                            ],
                                                          ),
                                                          SizedBox(height: 8.0),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Flexible(
                                  flex: 2,
                                  child: Container(
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      // color: Colors.black,
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          // color: Colors.amber,
                                          child: Row(
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.all(4),
                                                child: Card(
                                                  elevation: 10,
                                                  child: Container(
                                                    height: 22,
                                                    width: 22,
                                                    child: Image.asset(
                                                      "assets/male.png",
                                                      fit: BoxFit.contain,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.all(4),
                                                child: Container(
                                                  child: Text(
                                                    "Male",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontFamily: "Raleway",
                                                        fontSize: 12),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.all(4),
                                                child: widget._places.male == 1
                                                    ? Icon(Icons.check_box,
                                                        color: Colors.green)
                                                    : Icon(Icons.close,
                                                        color: Colors.red),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.all(4),
                                              child: Card(
                                                elevation: 10,
                                                child: Container(
                                                  height: 22,
                                                  width: 22,
                                                  child: Image.asset(
                                                    "assets/female.png",
                                                    fit: BoxFit.contain,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(4),
                                              child: Container(
                                                child: Text(
                                                  "Female",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontFamily: "Raleway",
                                                      fontSize: 12),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(4),
                                              child: widget._places.female == 1
                                                  ? Icon(Icons.check_box,
                                                      color: Colors.green)
                                                  : Icon(Icons.close,
                                                      color: Colors.red),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.all(4),
                                              child: Card(
                                                elevation: 10,
                                                child: Container(
                                                  height: 22,
                                                  width: 22,
                                                  child: Image.asset(
                                                    "assets/female.png",
                                                    fit: BoxFit.contain,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(4),
                                              child: Container(
                                                child: Text(
                                                  "Female Friendly",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontFamily: "Raleway",
                                                      fontSize: 12),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(4),
                                              child: widget._places
                                                          .female_friendly ==
                                                      1
                                                  ? Icon(Icons.check_box,
                                                      color: Colors.green)
                                                  : Icon(Icons.close,
                                                      color: Colors.red),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.all(4),
                                              child: Card(
                                                elevation: 10,
                                                child: Container(
                                                  height: 22,
                                                  width: 22,
                                                  child: Image.asset(
                                                    "assets/disable.png",
                                                    fit: BoxFit.contain,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(4),
                                              child: Container(
                                                child: Text(
                                                  "Disable Access",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontFamily: "Raleway",
                                                      fontSize: 12),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(4),
                                              child: widget._places
                                                          .disableAccess ==
                                                      1
                                                  ? Icon(Icons.check_box,
                                                      color: Colors.green)
                                                  : Icon(Icons.close,
                                                      color: Colors.red),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            height: 32,
                            child: RaisedButton(
                              color: Colors.blue,
                              textColor: Colors.white,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    child: Icon(Icons.navigation),
                                    padding: EdgeInsets.only(right: 10.0),
                                  ),
                                  Text(
                                    'NAVIGATE',
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      fontFamily: "Raleway",
                                    ),
                                  ),
                                ],
                              ),
                              onPressed: () {
                                _launchMapsUrl(
                                    widget.location_lat, widget.location_long);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ]),
              )),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 40, right: 60),
              child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(70))),
                  child: Container(
                      clipBehavior: Clip.antiAlias,
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.blue.withOpacity(0.2),
                      ),
                      child: Center(
                        child: FittedBox(
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              "${(widget.distance * 1000).toInt()}\nm",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 13, fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      ))),
            ),
          ),
        ],
      ),
    );
  }

  initMarker() {
    //setPolylines();
    final MarkerId markerId = MarkerId('duka');
    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(widget.lat, widget.long),
      onTap: () {
        //_onMarkerTapped(markerId);
      },
    );
    setState(() {
      // adding a new marker to map
      markers[markerId] = marker;
    });
  }

  // setPolylines() async {
  //   //  print("ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
  //   List<PointLatLng> result = await polylinePoints?.getRouteBetweenCoordinates(
  //       "AIzaSyBuxJxY-gsbfOSnokla_PjOomY1VAroo5U",
  //       widget.lat,
  //       widget.long,
  //       widget._places.locaiton_lat,
  //       widget._places
  //           .location_long); //print("ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
  //   if (result.isNotEmpty && result.length != 0) {
  //     //  print("ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
  //     // loop through all PointLatLng points and convert them
  //     // to a list of LatLng, required by the Polyline
  //     result.forEach((PointLatLng point) {
  //       polylineCoordinates.add(LatLng(point.latitude, point.longitude));
  //     });
  //   }
  //   setState(() {
  //     // create a Polyline instance
  //     // with an id, an RGB color and the list of LatLng pairs
  //     // Polyline polyline = Polyline(
  //     //     polylineId: PolylineId("poly"),
  //     //     color: Color.fromARGB(225, 40, 122, 198),
  //     //     points: polylineCoordinates);

  //     // add the constructed polyline as a set of points
  //     // to the polyline set, which will eventually
  //     // end up showing up on the map
  //     //_polylines.add(polyline);
  //   });
  // }

  void onMapCrated(controller) {
    setState(() {
      mapController = controller;

      // setPolylines();

      // Geolocator().placemarkFromAddress('Ghion Hotel');
    });
  }

  Completer<GoogleMapController> _controller = Completer();
  // 2
  static final CameraPosition _myLocation = CameraPosition(
    target: LatLng(0, 0),
  );
}
