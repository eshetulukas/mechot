import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_map_polyline/google_map_polyline.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission/permission.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:sanitation/models/place_model.dart';

class RouteMap extends StatefulWidget {
  final startLat;
  final startLong;
  PlaceDetail _places;
RouteMap(this.startLat,this.startLong,this._places);
  @override
  _RouteMapState createState() => _RouteMapState();
}
class _RouteMapState extends State<RouteMap> {
  final LatLng _officeLatLng = const LatLng(13.0215605, 77.6378041);
  GoogleMapController mapController;
  final Set<Polyline> poly = {};
  //List<LatLng> latlng = List();
  //final LatLng _officeLatLng = const LatLng(13.0215605, 77.6378041);
  // GoogleMapController mapController;
  //final Set<Polyline> poly = {};
  List<LatLng> latlng = List();
  LatLng _originLatLng = LatLng(13.01463, 77.63556500000004);
  LatLng _destinationLatLng = LatLng(13.0216685, 77.63998420000007);

  String _originPlaceId = "ORIGIN PLACE ID";
  String _destinationPlaceId = "DESTINATION PLACE ID";

  void _onMapCreated(GoogleMapController controller) {
    latlng.add(_originLatLng);
    latlng.add(_originLatLng);
    mapController = controller;
    poly.add(Polyline(
        polylineId: PolylineId(_officeLatLng.toString()),
        points: latlng,
        color: Colors.orange,
        width: 3));
  }

  Widget _googleMap(BuildContext context) {
    return Container(
      child: GoogleMap(
        initialCameraPosition:
            CameraPosition(target: _officeLatLng, zoom: 15.0),
        mapType: MapType.normal,
        onMapCreated: _onMapCreated,
        polylines: poly,
        zoomGesturesEnabled: true,
        myLocationButtonEnabled: true,
        myLocationEnabled: true,
        mapToolbarEnabled: true,
        compassEnabled: true,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Google Map")),
      body: Stack(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            
            _googleMap(context),
            Expanded(
              child: RaisedButton(
                color: Colors.blue,
                textColor: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      child: Icon(Icons.navigation),
                      padding: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'NAVIGATE',
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ],
                ),
                onPressed: () {
                  _launchMapsUrl(widget._places.name);
                },
              ),
            ),
          ]),
    );
  }

  void _launchMapsUrl(String destinationPlaceName) async {
  
    final url = 'https://www.google.com/maps/dir/?api=1&destination=$destinationPlaceName';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
