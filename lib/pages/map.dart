import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sanitation/models/place_detail_model.dart';
import 'package:sanitation/models/place_model.dart';

class GoogleMapPage extends StatefulWidget {
  double lat;
  double long;
 var _places;
  var points;
  GoogleMapPage(this.lat, this.long, [this._places]) {
    points = LatLng(lat, long);
  }

  @override
  _GoogleMapPageState createState() => _GoogleMapPageState();
}
class _GoogleMapPageState extends State<GoogleMapPage> {
  GoogleMapController mapController;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Set<Marker> createMarker() {
    var allMarkers = Set<Marker>();
    allMarkers.add(
      Marker(
        markerId: MarkerId("posi"),
        position: widget.points,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure),
         infoWindow: InfoWindow(title:"My location")
      ),
    );
    if (widget._places != null) {
      for (int i = 0; i < widget._places.length; i++) {
        allMarkers.add(
          Marker(
              markerId: MarkerId(widget._places[i].name.toString()),
              position: LatLng(widget._places[i].locaiton_lat,
                  widget._places[i].location_long),
                 // icon:  pinLocationIcon,
              infoWindow: InfoWindow(
                title: widget._places[i].name.toString(),
              )
              ),
        );
      }
    }

    return allMarkers;
  }
BitmapDescriptor pinLocationIcon;
  @override
  void initState() {
    BitmapDescriptor.fromAssetImage(
         ImageConfiguration(devicePixelRatio: 2.5),
         'assets/toilet.png').then((onValue) {
            pinLocationIcon = onValue;
         });
    // TODO: implement initState
    super.initState();
    initMarker();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        child: GoogleMap(
          mapType: MapType.normal,
            myLocationEnabled: true,
      compassEnabled: true,
          markers: createMarker(),
          initialCameraPosition:
              CameraPosition(target: LatLng(widget.lat, widget.long), zoom: 15),
        ));
  }
  initMarker() {
    final MarkerId markerId = MarkerId('duka');
    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(widget.lat, widget.long),
      onTap: () {
        //_onMarkerTapped(markerId);
      },
    );
    setState(() {
      // adding a new marker to map
      markers[markerId] = marker;
    }
    );
  }

  void onMapCrated(controller) {
    setState(() {
      mapController = controller;

      // Geolocator().placemarkFromAddress('Ghion Hotel');
    });
  }

  Completer<GoogleMapController> _controller = Completer();
  // 2
  static final CameraPosition _myLocation = CameraPosition(
    target: LatLng(0, 0),
  );
}
