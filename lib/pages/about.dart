import 'package:flutter/material.dart';

class About extends StatelessWidget {
  var drawerValue;
  About(this.drawerValue);

  scaffoldBody() {
    if (drawerValue == "About") {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.pink,
          title: Padding(
            padding: EdgeInsets.all(8),
            child: Text("About"),
          ),
        ),
        body: Container(
          color: Colors.pink,
          child: Column(children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: Container(
                height: 150,
                width: 150,
                child: Image.asset(
                  "assets/Mechot.png",
                  fit: BoxFit.contain,
                  // color: Colors.black,
                ),
              ),
            ),
            Padding(
                padding: const EdgeInsets.all(15.0),
                child: Text(
                  "MECHOT is the Amharic word for comfort \n\nWe are MECHOT, the first plaform that offers public health and security information.",
                  style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                      fontFamily: "Raleway",
                      fontWeight: FontWeight.normal),
                ))
          ]),
        ),
      ),
    );
  }
}
