import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:sanitation/pages/drawerPage.dart';
import 'package:sanitation/pages/placeDetail_page.dart';
import 'package:sanitation/pages/places_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cached_network_image/cached_network_image.dart';


import 'HomePageBackground.dart';

class Search extends StatefulWidget {
  @override
  _ForMeState createState() => _ForMeState();
}

class _ForMeState extends State<Search> {
  bool isSettingTapped = false;
  final String logeIn = "in";
  final bool disableAdds = false;
  final String ads = "ads";
  bool isFirstTime = true;
  bool addValue = true;

  /// ------------------------------------------------------------
  /// Method that returns the user decision to allow notifications
  /// ------------------------------------------------------------
  Future<bool> getAllowsNotifications() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool(ads);
  }

  Future<bool> getLogin() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
  }

  bool getNotification() {
    setState(() {
      getAllowsNotifications().then((value) {
        setState(() {
          value != null ? addValue = value : addValue = addValue;
          return addValue;
        });
      });
    });
    return false;
  }

  List allAds;
  getAds() {
    http.get("http://35.246.213.39:81/api/ad/all",
        headers: {"Accept": "application/json"}).then((value) {
      setState(() {
        if (value.statusCode == 200) {
          allAds = json.decode(value.body)['message'];
        }
      });
    });
  }

//  FirebaseMessaging fb = new FirebaseMessaging();
  @override
  void initState() {
    super.initState();
    // fb.configure(
    //   onMessage: (Map<String, dynamic> message) {
    //     print(message.toString());
    //   },
    //   onResume: (Map<String, dynamic> message) {
    //     print(message.toString());
    //   },
    //   onLaunch: (Map<String, dynamic> message) {
    //     print(message.toString());
    //   },
    // );
    // fb.getToken().then((value) {
    //   print(value);
    // });

    getAllowsNotifications().then((value) {
      setState(() {
        value != null ? addValue = value : addValue = addValue;
      });
    });
    getAds();
  }

  bool istaped = false;
  List<String> place = new List(4);
  List<String> menu = new List(2);
  List<String> names = new List(3);
  String dropdownValue = 'Police Stations';
  createAlert(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white10,
            title: istaped == true
                ? Container(
                    //color: Colors.black,
                    child: Center(
                        child: Text('done!',
                            style:
                                TextStyle(fontSize: 20, color: Colors.white))),
                  )
                : Center(
                    child: SpinKitCircle(
                      color: Colors.white,
                      size: 60,
                    ),
                  ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    place[0] = ("bar");
    place[1] = ("police");
    place[2] = ("hospital");
    place[3] = ("pharmacy");
    names[0] = ("Police Stations");
    names[1] = ("Hospitals        ");
    names[2] = ("Pharmacies       ");

    void popUp() {
      showDialog(
        context: context,
        builder: (context) => Container(
          height: MediaQuery.of(context).size.height,
          color: Colors.transparent,
          child: AlertDialog(
            title: Text(
              'Are you sure?',
              style:
                  TextStyle(fontFamily: "Raleway", fontWeight: FontWeight.bold),
            ),
            content: Text(
              'Do you want to exit the App?',
              style: TextStyle(fontFamily: "Raleway"),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text(
                  'No',
                  style: TextStyle(fontFamily: "Raleway"),
                ),
              ),
              FlatButton(
                onPressed: () => exit(0),
                /*Navigator.of(context).pop(true)*/
                child: Text(
                  'Yes',
                  style: TextStyle(fontFamily: "Raleway"),
                ),
              ),
            ],
          ),
        ),
      );
    }

    return WillPopScope(
      onWillPop: () {
        popUp();
      },
      child: Scaffold(
        backgroundColor: Colors.pink,
        appBar: GradientAppBar(
          elevation: 0,
          gradient: LinearGradient(
            colors: <Color>[
              Colors.pink,
              Colors.pink,
            ],
          ),
          // leading: Padding(
          //   padding: EdgeInsets.all(8),
          //   child: Image.asset(
          //     "assets/mechot (1).jpg",
          //     height: 20,
          //     width: 20,
          //   ),
          // ),
        ),
        drawer: DrawerPage(),
        body: Stack(
          children: <Widget>[
            new HomePageBacground(
                screenHeight: MediaQuery.of(context).size.height / 2),
            Container(
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.5,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Expanded(child: SizedBox(height: 30)),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(new MaterialPageRoute(
                                  builder: (BuildContext context) {
                                return PlacesPage(place[0], 1,
                                    "http://35.246.213.39:81/storage/${allAds[3]['picture_url']}");
                              }));
                            },
                            child: Image.asset(
                              'assets/to.png',
                              height: 200,
                              width: 200,

                              fit: BoxFit.cover,

                              // color: Colors.black26,
                              colorBlendMode: BlendMode.darken,
                            ),
                          ),
                          Container(
                            child: Text(
                              "Locate Nearest Toilet",
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Expanded(
                            child: SizedBox(
                              height: 50,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    child: Column(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              child: Row(
                                // crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(10, 20, 0, 7),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Just In Case',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: "Poppins",
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 20),
                                    child: Container(
                                      color: Colors.pink,
                                      child: Card(
                                        elevation: 10,
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 8),
                                          child: DropdownButton<String>(
                                            value: dropdownValue,
                                            icon: Icon(
                                              Icons.expand_more,
                                              color: Colors.pink,
                                            ),
                                            iconSize: 24,
                                            elevation: 0,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18),
                                            // underline: Container(
                                            //   height: 2,
                                            //   color: Colors.black,
                                            // ),
                                            onChanged: (String newValue) {
                                              Navigator.pushReplacement(
                                                context,
                                                MaterialPageRoute(builder:
                                                    (BuildContext context) {
                                                  if (newValue ==
                                                      'Police Stations') {
                                                    return PlaceDetailPage(
                                                        place[1], 1);
                                                  } else if (newValue ==
                                                      'Hospitals') {
                                                    return PlaceDetailPage(
                                                        place[2], 2);
                                                  } else {
                                                    return PlaceDetailPage(
                                                        place[3], 3);
                                                  }
                                                }),
                                              );
                                            },
                                            items: <String>[
                                              'Police Stations',
                                              'Hospitals',
                                              'Pharmacies ',
                                            ].map<DropdownMenuItem<String>>(
                                                (String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: Container(
                                                    child: Text(value,
                                                        style: TextStyle(
                                                          fontFamily: "Raleway",
                                                        ))),
                                              );
                                            }).toList(),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                  //kkkkk
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5.0, vertical: 0),
                              child: Container(
                                height: 0,
                                width: double.infinity,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: SizedBox(
                      height: 10,
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: addValue && allAds != null
                  ? Container(
                      //color: Colors.amber,
                      height: MediaQuery.of(context).size.height * 0.12,
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.7,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal:10,vertical:8),
                              child: CachedNetworkImage(
                                height: 500,
                                placeholder: (context, url) =>
                                    SpinKitThreeBounce(
                                  color: Colors.white,
                                  size: 10,
                                ),
                                imageUrl:
                                    'http://35.246.213.39:81/storage/${allAds[0]['picture_url']}',
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.3,
                            child: FittedBox(
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 20),
                                    child: CachedNetworkImage(
                                      height: 500,
                                      placeholder: (context, url) =>
                                          SpinKitThreeBounce(
                                        color: Colors.white,
                                        size: 10,
                                      ),
                                      imageUrl:
                                          'http://35.246.213.39:81/storage/${allAds[1]['picture_url']}',
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        right: 20, bottom: 70),
                                    child: CachedNetworkImage(
                                      placeholder: (context, url) =>
                                          SpinKitThreeBounce(
                                        color: Colors.white,
                                        size: 10,
                                      ),
                                      imageUrl:
                                          'http://35.246.213.39:81/storage/${allAds[2]['picture_url']}',
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(),
            )
          ],
        ),
      ),
    );
  }
}
