import 'dart:math';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:sanitation/pages/setting.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login_page extends StatefulWidget {
  Login_page({Key key}) : super(key: key);

  @override
  _Login_pageState createState() => _Login_pageState();
}

class _Login_pageState extends State<Login_page>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);
    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {}
    });
    controller.forward();
  }
  String userNameInput;
  String userPassword;
  String userName = "mechot";
  String password = "mechot@12345";
  bool error = false;
  final String logeIn = "in";
  final bool disableAdds = false;
  final String ads = "ads";

  /// ------------------------------------------------------------
  /// Method that returns the user decision to allow notifications
  /// ------------------------------------------------------------
  Future<bool> getAllowsNotifications() async {
	final SharedPreferences prefs = await SharedPreferences.getInstance();

  	return prefs.getBool(ads);
  }
   Future<bool> getLogin() async {
	final SharedPreferences prefs = await SharedPreferences.getInstance();

  	return prefs.getString(logeIn)=="in"?? false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the user decision to allow notifications
  /// ----------------------------------------------------------
  Future<bool> setAllowsNotifications({bool value,String login}) async {
	final SharedPreferences prefs = await SharedPreferences.getInstance();

      prefs.setBool(ads, value);
      prefs.setString(logeIn, login);
  }


  Widget build(BuildContext context) {
    return Scaffold(
      //  backgroundColor: Colors.pink,
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: Padding(
          padding: EdgeInsets.all(8),
          child: Text("Login"),
        ),
      ),
      body: Center(
        child: FadeTransition(
          opacity: animation,
          child: Container(
            height: MediaQuery.of(context).size.height * 0.75,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Card(
                clipBehavior: Clip.antiAlias,
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Material(
                  borderRadius: BorderRadius.circular(20.0),
                  elevation: 60,
                  shadowColor: Colors.black45,
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        color: Colors.pink,
                        child: Container(
                          // duration: Duration(seconds: 3),
                          //curve: Curves.easeIn,
                          height: 100,
                          width: 100,
                          child: Center(
                            child: Image.asset("assets/Mechot.png"),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 45,
                      ),
                      Expanded(
                                              child: SingleChildScrollView(
                          child: Column(children: <Widget>[
                             Padding(
                          padding: const EdgeInsets.fromLTRB(50, 5, 50, 30),
                          child: Container(
                            width: double.infinity,
                            child: Text(
                              "LOGIN",
                              textAlign: TextAlign.start,
                              style: TextStyle(color: Colors.black, fontSize: 20),
                            ),
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 50),
                          child: FadeTransition(
                            opacity: animation,
                            child: TextField(
                               onChanged: (input) {
                          setState(() {
                           error = false;
                          });
                          userNameInput = input;
                        },
                              decoration: InputDecoration(
                                labelStyle: TextStyle(
                                  color: Colors.black,
                                ),
                                prefixIcon: Padding(
                                  padding:
                                      const EdgeInsetsDirectional.only(end: 15.0),
                                  child: Icon(Icons.person, color: Colors.black),
                                ),
                                
                                labelText: 'User name',
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 50),
                          child: FadeTransition(
                            opacity: animation,
                            child: TextField(
                              obscureText: true,
                                  onChanged: (input) {
                          setState(() {
                           error = false;
                          });
                          userPassword = input;
                        },
                              decoration: InputDecoration(
                                  labelStyle: TextStyle(
                                    color: Colors.black,
                                  ),
                                  prefixIcon: Padding(
                                    padding: const EdgeInsetsDirectional.only(
                                        end: 15.0),
                                    child: Icon(Icons.lock, color: Colors.black),
                                  ),
                                  labelText: 'Password'),
                            ),
                          ),
                        ),
                        error? Padding(
                                padding:
                                    EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                                child: GestureDetector(
                                  onTap: () {
                                    print("onTap called.");
                                  },
                                  child: Container(
                                    width: double.infinity,
                                    child: Text(
                                      "user name or password is not corret",
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                        color: Colors.red,
                                      ),
                                    ),
                                  ),
                                ),
                              ):Container(),
                        SizedBox(
                          height: 1,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 80, vertical: 8),
                          child: RaisedButton(
                            onPressed: () {
                              setState(() {
                                 if(userPassword==password && userNameInput==userName)
                              {
                                setAllowsNotifications(login:"in");
                                 Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) {
                          return Setting();
                        }));
                              }
                              else error = true;
                              });
                             
                            },
                            padding: const EdgeInsets.all(0.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                            ),
                            child: Container(
                              width: double.infinity,
                              decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    Colors.pink,
                                    Colors.pink,
                                  ],
                                ),
                              ),
                              padding: const EdgeInsets.all(10.0),
                              child: const Text(
                                'Login',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                          ]),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                      // Transform.rotate(
                      //   angle: pi,
                      //   child: Login_page_background_bottom(
                      //       screenHeight:
                      //           MediaQuery.of(context).size.height /
                      //               3.5),
                      // ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
