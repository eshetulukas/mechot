import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';

class Setting extends StatefulWidget {
  Setting({Key key}) : super(key: key);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  String name;
  String address;
  String dropdownValue = 'Cafe';
  double lat = 0;
  double long = 0;
  int male = 0;
  int female = 0;
  int femaleFriendly = 0;
  int disableAccess = 0;
  bool checkBoxValue1 = false;
  bool checkBoxValue2 = false;
  bool checkBoxValue3 = false;
  bool checkBoxValue4 = false;
  final text = TextEditingController();
  final text2 = TextEditingController();
  final text3 = TextEditingController();
  bool validate = false;
  bool validate2 = false;
  bool validate3 = false;
  bool enabled = false;
  List<DropdownMenuItem<int>> category = [];
  Future<LocationData> getLocation() async {
    bool enabled = await Location().serviceEnabled();
    var location = await Location().getLocation();
    return location;
  }

  bool istaped = false;
  String token;
  createAlert(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            // backgroundcolor: Colors.white10,
            title: istaped == true
                ? Container(
                    //color: Colors.black,
                    child: Center(
                        child: Text('done!',
                            style:
                                TextStyle(fontSize: 16, color: Colors.black))),
                  )
                : Center(
                    child: SpinKitCircle(
                      color: Colors.black,
                      size: 60,
                    ),
                  ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    Location().serviceEnabled().then((value) {
      if (value) {
        enabled = value;
        getLocation().then((value) {
          lat = value.latitude;
          long = value.longitude;
          setState(() {
            print(lat.toString());
            print(long.toString());
          });
        });
      } else {
        Location().requestService().then((value) {
          if (value) {
            enabled = value;
            getLocation().then((value) {
              lat = value.latitude;
              long = value.longitude;
              setState(() {
                print(lat.toString());
                print(long.toString());
              });
            });
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    void popUp() {
      showDialog(
        context: context,
        builder: (context) => Container(
          color: Colors.transparent,
          child: AlertDialog(
            title: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Detail'),
            ),
            content: Container(
              height: MediaQuery.of(context).size.height / 1.5,
              width: MediaQuery.of(context).size.width * 0.95,
              child: SingleChildScrollView(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Card(
                              elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                  width: double.infinity,
                                  // color: Colors.black,
                                  child: RichText(
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: 'Name:     ',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold)),
                                        TextSpan(
                                            text: '$name',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w300)),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Card(
                              elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                  width: double.infinity,
                                  child: RichText(
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: 'Address:     ',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold)),
                                        TextSpan(
                                            text: '$address',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w300)),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),

                            // Card(
                            //   elevation: 10,
                            //   child: Padding(
                            //     padding: const EdgeInsets.all(20.0),
                            //     child: Container(
                            //       width: double.infinity,
                            //       child: RichText(
                            //         text: TextSpan(
                            //           children: <TextSpan>[
                            //             TextSpan(
                            //                 text: 'Sub City:     ',
                            //                 style: TextStyle(
                            //                     color: Colors.black,
                            //                     fontSize: 16,
                            //                     fontWeight: FontWeight.bold)),
                            //             TextSpan(
                            //                 text: '$subCity',
                            //                 style: TextStyle(
                            //                     color: Colors.black,
                            //                     fontSize: 16,
                            //                     fontWeight: FontWeight.w300)),
                            //           ],
                            //         ),
                            //       ),
                            //     ),
                            //   ),
                            // ),
                            Card(
                              elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                  width: double.infinity,
                                  child: RichText(
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: 'Category:     ',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold)),
                                        TextSpan(
                                            text: '$dropdownValue',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w300)),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Card(
                              elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                  width: double.infinity,
                                  child: RichText(
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: 'Male:     ',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold)),
                                        TextSpan(
                                            text: checkBoxValue1 ? 'Yes' : "No",
                                            style: TextStyle(
                                                color: checkBoxValue1
                                                    ? Colors.green
                                                    : Colors.red,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w300)),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Card(
                              elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                  width: double.infinity,
                                  child: RichText(
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: 'Female:     ',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold)),
                                        TextSpan(
                                            text: checkBoxValue2 ? 'Yes' : "No",
                                            style: TextStyle(
                                                color: checkBoxValue2
                                                    ? Colors.green
                                                    : Colors.red,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w300)),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Card(
                              elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                  width: double.infinity,
                                  child: RichText(
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: 'Disable Access:     ',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold)),
                                        TextSpan(
                                            text: checkBoxValue3 ? 'Yes' : "No",
                                            style: TextStyle(
                                                color: checkBoxValue3
                                                    ? Colors.green
                                                    : Colors.red,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w300)),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Card(
                              elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                  width: double.infinity,
                                  child: RichText(
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: 'Female Friendly:     ',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold)),
                                        TextSpan(
                                            text: checkBoxValue4 ? 'Yes' : "No",
                                            style: TextStyle(
                                                color: checkBoxValue4
                                                    ? Colors.green
                                                    : Colors.red,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w300)),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                child: RaisedButton(
                  onPressed: () async {
                    createAlert(context);
                    http.post("http://35.246.213.39:81/admin.api/login", body: {
                      "username": "admin",
                      "password": "goldg_123"
                    }).then((value) {
                      if (value.statusCode == 200) {
                        token = json.decode(value.body)['token'];
                        print("$token");

                        http.post("http://35.246.213.39:81/admin.api/toilet",
                            headers: {
                              'Accept': 'application/json',
                              // 'Authorization': 'Bearer $token',
                              // HttpHeaders.authorizationHeader: "Bearer $token"
                            },
                            body: {
                              "name": "$name",
                              "address": "$address",
                              "category": "$dropdownValue",
                              "lat": "$lat",
                              "lng": "$long",
                              "female_friendly": "$femaleFriendly",
                              "male": "$male",
                              "female": "$female",
                              "disable_access": "$disableAccess"
                            }).then((value) {
                          if (value.statusCode == 200) {
                            print(value.body.toString());
                            print(
                                "duksssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
                            Navigator.of(context).pop(false);
                            setState(() {
                              istaped = true;
                              createAlert(context);
                            });
                          } else {
                            print(value.statusCode.toString());
                          }
                        });
                      } else {
                        print(value.statusCode.toString());
                      }
                    });

                    // Timer(Duration(seconds: 2), () {
                    //   Navigator.of(context).pop(false);
                    //   setState(() {
                    //      istaped = true;
                    //     //  createAlert(context);
                    //   });
                    // //  createAlert(context);
                    //   // Navigator.of(context).push(new MaterialPageRoute(
                    //   //     builder: (BuildContext context) {
                    //   //   return Search();
                    //   // }));
                    // });
                    // var reponse = await http
                    //     .post("http://35.246.213.39:81/admin.api/login", body: {
                    //       "username":"admin",
                    //       "password":"123456"
                    //   // "name": "hello",
                    //   // "adddress":"$address",
                    //   // "category":"$dropdownValue",
                    //   // "lat":lat,
                    //   // "long":long,
                    //   // "female_friendly":femaleFriendly,
                    //   // "male":male,
                    //   // "female":female,
                    //   // "disable_access":disableAccess
                    // }).then((value) => print(value.toString()));
                  },
                  padding: const EdgeInsets.all(0.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                  child: Container(
                    width: 90,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      gradient: LinearGradient(
                        colors: <Color>[
                          Colors.pink,
                          Colors.pink,
                        ],
                      ),
                    ),
                    padding: const EdgeInsets.all(10.0),
                    child: const Text(
                      'Save',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.blueAccent),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  width: 90,
                  //height: 50,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Edit',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.pink,
      appBar: GradientAppBar(
        elevation: 0,
        gradient: LinearGradient(
          colors: <Color>[Colors.pink, Colors.pink],
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
     
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Card(
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.all(8),
                  child: Container(
                      height: 70,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Row(children: <Widget>[
                                  Icon(
                                    Icons.location_on,
                                    color: Colors.red,
                                    size: 40,
                                  ),
                                  Text(
                                    " Latitude",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                  ),
                                ]),
                                enabled
                                    ? FittedBox(
                                        child: Text(
                                          "  $lat",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                          ),
                                        ),
                                      )
                                    : SpinKitThreeBounce(
                                        color: Colors.black45,
                                        size: 10,
                                      ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Row(children: <Widget>[
                                  Icon(
                                    Icons.location_on,
                                    color: Colors.red,
                                    size: 40,
                                  ),
                                  Text(
                                    " Longitude",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                  ),
                                ]),
                                enabled
                                    ? FittedBox(
                                        child: Text(
                                          "    $long",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                          ),
                                        ),
                                      )
                                    : SpinKitThreeBounce(
                                        color: Colors.black45,
                                        size: 10,
                                      ),
                              ],
                            ),
                          ),
                        ],
                      )

                      //   ListTile(
                      //       leading: Container(
                      //         width: MediaQuery.of(context).size.width / 2,
                      //         child: Column(
                      //           mainAxisAlignment: MainAxisAlignment.start,
                      //           children: <Widget>[
                      //             Row(children:<Widget>[
                      //                Icon(
                      //               Icons.location_on,
                      //               color: Colors.red,
                      //               size: 40,
                      //             ),
                      //             Text(
                      //               " Latitude",
                      //               style: TextStyle(
                      //                 color: Colors.black,
                      //                 fontSize: 16,
                      //               ),
                      //             ),
                      //             ]),

                      //             enabled
                      //                 ? FittedBox(
                      //                     child: Text(
                      //                       " Latitude: $lat",
                      //                       style: TextStyle(
                      //                         color: Colors.black,
                      //                         fontSize: 16,
                      //                       ),
                      //                     ),
                      //                   )
                      //                 : SpinKitThreeBounce(
                      //                     color: Colors.black45,
                      //                     size: 10,
                      //                   ),
                      //           ],
                      //         ),
                      //       ),
                      //       trailing: Container(
                      //         width: MediaQuery.of(context).size.width / 2,
                      //         child: Row(
                      //           mainAxisAlignment: MainAxisAlignment.end,
                      //           children: <Widget>[
                      //             Icon(
                      //               Icons.location_on,
                      //               color: Colors.red,
                      //               size: 40,
                      //             ),
                      //             enabled
                      //                 ? FittedBox(
                      //                     child: Text(
                      //                       " Longitued: $long",
                      //                       style: TextStyle(
                      //                         color: Colors.black,
                      //                         fontSize: 16,
                      //                       ),
                      //                     ),
                      //                   )
                      //                 : SpinKitThreeBounce(
                      //                     color: Colors.black45,
                      //                     size: 10,
                      //                   ),
                      //           ],
                      //         ),
                      //       )),
                      ),
                ),
              ),
              SizedBox(height: 15),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: TextField(
                        controller: text,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        onChanged: (input) {
                          setState(() {
                            validate = false;
                          });
                          name = input;
                        },
                        decoration: InputDecoration(
                          errorStyle: TextStyle(color: Colors.white),
                          errorText:
                              validate ? "This field can't be empty" : null,
                          labelStyle: TextStyle(
                            color: Colors.white,
                          ),
                          // prefixIcon: Padding(
                          //   padding:
                          //       const EdgeInsetsDirectional.only(end: 15.0),
                          //   child: Icon(Icons.person, color: Colors.black),
                          // ),
                          labelText: 'Name',
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: TextField(
                        cursorColor: Colors.white,
                        
                        style: TextStyle(color: Colors.white),
                        controller: text2,
                        onChanged: (input) {
                          setState(() {
                            validate2 = false;
                          });
                          address = input;
                        },
                        decoration: InputDecoration(
                           errorStyle: TextStyle(color: Colors.white),
                          errorText:
                              validate2 ? "This field can't be empty" : null,
                          labelStyle: TextStyle(
                            color: Colors.white,
                          ),
                          // prefixIcon: Padding(
                          //   padding:
                          //       const EdgeInsetsDirectional.only(end: 15.0),
                          //   child: Icon(Icons.person, color: Colors.black),
                          // ),
                          labelText: 'Address',
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Text(
                          "Category",
                          style: TextStyle(fontSize: 16, color: Colors.white),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        child: Card(
                          elevation: 10,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: DropdownButton<String>(
                              value: dropdownValue,
                              icon: Icon(
                                Icons.expand_more,
                                color: Colors.pink,
                              ),
                              iconSize: 24,
                              elevation: 0,
                              style:
                                  TextStyle(color: Colors.black, fontSize: 17),
                              // underline: Container(
                              //   height: 2,
                              //   color: Colors.black,
                              // ),
                              onChanged: (String newValue) {
                                setState(() {
                                  dropdownValue = newValue;
                                });
                              },
                              items: <String>[
                                'Cafe',
                                'Hotel',
                                'Mall',
                                'Other'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  // Expanded(
                  //   child: Padding(
                  //     padding: const EdgeInsets.symmetric(horizontal: 50),
                  //     // child: TextField(
                  //     //   controller: text3,
                  //     //   onChanged: (input) {
                  //     //     setState(() {
                  //     //       validate3 = false;
                  //     //     });
                  //     //     subCity = input;
                  //     //   },
                  //     //   decoration: InputDecoration(
                  //     //     errorText:
                  //     //         validate3 ? "This field can't be empty" : null,
                  //     //     labelStyle: TextStyle(
                  //     //       color: Colors.black,
                  //     //     ),
                  //     //     // prefixIcon: Padding(
                  //     //     //   padding:
                  //     //     //       const EdgeInsetsDirectional.only(end: 15.0),
                  //     //     //   child: Icon(Icons.person, color: Colors.black),
                  //     //     // ),
                  //     //     labelText: 'Sub city',
                  //     //   ),
                  //     // ),
                  //   ),
                  // ),
                ],
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 14, vertical: 20),
                    child: Column(
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Card(
                                    elevation: 10,
                                    child: Container(
                                      height: 55,
                                      width: 55,
                                      child: Image.asset(
                                        "assets/male.png",
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                                                  child: Padding(
                                    padding: EdgeInsets.all(8),
                                    child: Container(
                                      child: Text(
                                        "Male                     ",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 17),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Container(
                                     width: 20,
                                    child: Checkbox(
                                      value: checkBoxValue1,

                                      focusColor: Colors.white,

                                      // Colors.white,
                                      onChanged: (val) {
                                        setState(() {
                                          checkBoxValue1 = val;
                                          if (checkBoxValue1) {
                                            male = 1;
                                          }
                                        });
                                      },
                                      activeColor: Colors.green,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Card(
                                    elevation: 10,
                                    child: Container(
                                      height: 55,
                                      width: 55,
                                      child: Image.asset(
                                        "assets/female.png",
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                                                  child: Padding(
                                    padding: EdgeInsets.all(8),
                                    child: Container(
                                      child: Text(
                                        "Female                 ",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 17),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Container(
                                     width: 20,
                                    child: Checkbox(
                                      value: checkBoxValue2,
                                      onChanged: (val) {
                                        setState(() {
                                          checkBoxValue2 = val;
                                          if (checkBoxValue2) {
                                            female = 1;
                                          }
                                        });
                                      },
                                      activeColor: Colors.green,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Card(
                                    elevation: 10,
                                    child: Container(
                                      height: 55,
                                      width: 55,
                                      child: Image.asset(
                                        "assets/disable.png",
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                                                  child: Padding(
                                    padding: EdgeInsets.all(8),
                                    child: Container(
                                      child: Text(
                                        "Disable Access   ",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 17),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Container(
                                     width: 20,
                                    child: Checkbox(
                                      value: checkBoxValue3,
                                      onChanged: (val) {
                                        setState(() {
                                          checkBoxValue3 = val;
                                          if (checkBoxValue3) {
                                            disableAccess = 1;
                                          }
                                        });
                                      },
                                      activeColor: Colors.green,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Card(
                                    elevation: 10,
                                    child: Container(
                                      height: 55,
                                      width: 55,
                                      child: Image.asset(
                                        "assets/female.png",
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                                                  child: Padding(
                                    padding: EdgeInsets.all(8),
                                    child: Container(
                                     
                                      child: Text(
                                        "Female Friendly  ",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 17),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Container(
                                     width: 20,
                                    child: Checkbox(
                                      value: checkBoxValue4,
                                      onChanged: (val) {
                                        setState(() {
                                          checkBoxValue4 = val;
                                          if (checkBoxValue4) {
                                            femaleFriendly = 1;
                                          }
                                        });
                                      },
                                      activeColor: Colors.green,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 80, vertical: 50),
                          child: RaisedButton(
                            onPressed: () {
                              setState(() {
                                if (!enabled) {
                                  Location().serviceEnabled().then((value) {
                                    if (value) {
                                      enabled = value;
                                      getLocation().then((value) {
                                        lat = value.latitude;
                                        long = value.longitude;
                                        setState(() {
                                          print(lat.toString());
                                          print(long.toString());
                                        });
                                      });
                                    } else {
                                      Location().requestService().then((value) {
                                        if (value) {
                                          enabled = value;
                                          getLocation().then((value) {
                                            lat = value.latitude;
                                            long = value.longitude;
                                            setState(() {
                                              print(lat.toString());
                                              print(long.toString());
                                            });
                                          });
                                        }
                                      });
                                    }
                                  });
                                }

                                if (text.text.isEmpty) validate = true;
                                if (text2.text.isEmpty) validate2 = true;

                                if (checkBoxValue1) male = 1;
                                if (checkBoxValue2) female = 1;
                                if (checkBoxValue3) disableAccess = 1;
                                if (checkBoxValue4) femaleFriendly = 1;

                                // print(male.toString());
                                print(dropdownValue);
                                // print(male.toString());
                              });
                              if ((!validate && !validate2 && enabled)) popUp();
                            },
                            padding: const EdgeInsets.all(0.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                            ),
                            child: Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                border:
                                    Border.all(color: Colors.white, width: 2),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                gradient: LinearGradient(
                                  colors: <Color>[Colors.pink, Colors.pink],
                                ),
                              ),
                              padding: const EdgeInsets.all(10.0),
                              child: const Text(
                                'Submit ',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
