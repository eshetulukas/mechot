import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:sanitation/pages/HomePageBackground.dart';
import 'package:sanitation/pages/about.dart';
import 'package:sanitation/pages/login_page.dart';
import 'package:sanitation/pages/search.dart';
import 'package:sanitation/pages/setting.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawerPage extends StatefulWidget {
  DrawerPage({Key key}) : super(key: key);

  @override
  _DrawerPageState createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  bool istaped = true;

  bool isSettingTapped = false;
  final String logeIn = "in";
  final bool disableAdds = false;
  final String ads = "ads";
  bool isFirstTime = true;
  bool checkBoxValue1 = true;

  /// ------------------------------------------------------------
  /// Method that returns the user decision to allow notifications
  /// ------------------------------------------------------------
  Future<bool> getAllowsNotifications() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool(ads);
  }

  Future<bool> getLogin() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(logeIn) == "in" ?? false;
  }
  Future<bool> setAllowsNotifications({bool value, String login}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(ads, value);
    prefs.setString(logeIn, login);
  }
  createAlert(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white10,
            title: istaped == false
                ? Container(
                    //color: Colors.black,
                    child: Center(
                        child: Text('done!',
                            style:
                                TextStyle(fontSize: 20, color: Colors.white))),
                  )
                : Center(
                    child: SpinKitCircle(
                      color: Colors.white,
                      size: 60,
                    ),
                  ),
          );
        });
  }
  @override
  void initState() {
    super.initState();
    getAllowsNotifications().then((value) {
      setState(() {
       value!=null? checkBoxValue1 = value:checkBoxValue1=checkBoxValue1;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: const EdgeInsets.all(0.0),
            child: Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        //HomePageBacground(screenHeight: 480),
                        Container(
                          color: Colors.pink,
                          height: 200,
                          child: Center(
                            child: Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 35,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(6.0),
                                  child: Container(
                                    height: 150,
                                    width: 150,
                                    child: Image.asset(
                                      "assets/Mechot.png",
                                      fit: BoxFit.contain,
                                      // color: Colors.black,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 45),
                    GestureDetector(
                      onTap: () {
                        // createAlert(context);
                        setState(() {
                          if (isSettingTapped)
                            isSettingTapped = false;
                          else
                            isSettingTapped = true;
                        });

                        // Navigator.of(context).push(new MaterialPageRoute(
                        //     builder: (BuildContext context) {
                        //   return Setting();
                        // }));

                        //  Navigator.of(context).push(new MaterialPageRoute(
                        //     builder: (BuildContext context) {
                        //   return About("Check for Update");
                        // }));
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, bottom: 15),
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.settings, color: Colors.pink, size: 30),
                            SizedBox(width: 20),
                            Text(
                              "Setting",
                              style: TextStyle(fontSize: 17,fontFamily: "RaleWay"),
                            )
                          ],
                        ),
                      ),
                    ),
                    isSettingTapped
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(70, 0, 0, 4),
                            child: Container(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: GestureDetector(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                "Notifications  ",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.w600,
                                                    fontFamily: "RaleWay"),
                                              ),
                                              Checkbox(
                                                value: checkBoxValue1,
                                                focusColor: Colors.white,
                                                // Colors.white,
                                                onChanged: (val) {
                                                  setState(() {
                                                    if (checkBoxValue1)
                                                      {
                                                        setState(() {
                                                          checkBoxValue1 = false;
                                                        });
                                                        setAllowsNotifications(
                                                          value: false);
                                                      }
                                                      else
                                                      {
                                                         setState(() {
                                                          checkBoxValue1 = true;
                                                        });
                                                        setAllowsNotifications(
                                                          value: true);
                                                      }
                                                  });
                                                },
                                                activeColor: Colors.green,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 4),
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: GestureDetector(
                                        onTap: () {
                                          getLogin().then((value) {
                                            print(value.toString());
                                            setState(() {
                                              !value
                                                  ? Navigator.of(context).push(
                                                      new MaterialPageRoute(
                                                          builder: (BuildContext
                                                              context) {
                                                      return Login_page();
                                                    }))
                                                  : Navigator.of(context).push(
                                                      new MaterialPageRoute(
                                                          builder: (BuildContext
                                                              context) {
                                                      return Setting();
                                                    }));
                                            });
                                          });
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            "Add toilet",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600,
                                                fontFamily: "RaleWay"),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                            ),
                          )
                        : Container(),
                    SizedBox(height: 15),
                    GestureDetector(
                      onTap: () {
                        createAlert(context);
                        Timer(Duration(seconds: 3), () {
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) {
                            return Search();
                          }));
                        });
                        //  Navigator.of(context).push(new MaterialPageRoute(
                        //     builder: (BuildContext context) {
                        //   return About("Check for Update");
                        // }));
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, bottom: 15),
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.update, color: Colors.pink, size: 30),
                            SizedBox(width: 20),
                            Text(
                              "Check for Update",
                              style: TextStyle(
                                fontSize: 17,
                                fontFamily: "Raleway",
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) {
                          return About("About");
                        }));
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, bottom: 15),
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.error, color: Colors.pink, size: 30),
                            SizedBox(width: 20),
                            Text(
                              "About",
                              style: TextStyle(
                                fontSize: 17,
                                fontFamily: "Raleway",
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: RichText(
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Software Developed by ',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "Raleway",
                              fontSize: 14,
                            ),
                          ),
                          TextSpan(
                            text: 'GoldG telecom',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "Raleway",
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
