import 'dart:math';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:sanitation/models/place_detail_model.dart';
import 'package:sanitation/pages/direction.dart';
import 'package:sanitation/pages/map.dart';
import 'package:location/location.dart';
import 'package:sanitation/services/UserLocation.dart';
import 'package:http/http.dart' as http;

class PlacesPage extends StatefulWidget {
  var name;
  var index;
  String url;
  PlacesPage(this.name, this.index,this.url);
  @override
  State createState() => new Placestate();
}

class Placestate extends State<PlacesPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(body: _createContent());
  }

  // calculateDistance(double lat1, double lon1, double lat2, double lon2) {
  //   var p = 0.017453292519943295;
  //   var c = cos;
  //   double a = 0.5 -
  //       c((lat2 - lat1) * p) / 2 +
  //       c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
  //   double distance = (12742 * asin(sqrt(a)) * 1000);

  //   return distance;
  // }

  // Future<double> dis;
  bool notification = true;
  void setNotification() {
    setState(() {
      notification = false;
      print("hello");
    });
  }

  Widget _createContent() {
    void popUp() {
      showDialog(
        context: context,
        builder: (context) => Container(
          height: MediaQuery.of(context).size.height,
          color: Colors.transparent,
          child: AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit an App'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('No'),
              ),
            ],
          ),
        ),
      );
    }

    if (_places == null) {
      return Stack(
        children: <Widget>[
          GoogleMapPage(9, 30),
          Container(
            color: Colors.transparent,
            child: Center(
              child: SpinKitPulse(
                color: Colors.blue.withOpacity(0.2),
                size: 500,
                duration: Duration(seconds: 2),
              ),
            ),
          ),
        ],
      );
    } else if (_places.length == 0) {
      UserLocation.get().getNearbyPlaces().then((data) {
        this.setState(() {
          _places = data;
        });
      });
      return Stack(
        children: <Widget>[
          GoogleMapPage(9, 30),
          Container(
            color: Colors.transparent,
            child: Center(
              child: SpinKitPulse(
                color: Colors.blue.withOpacity(0.2),
                size: 500,
                duration: Duration(seconds: 2),
              ),
            ),
          ),
        ],
      );
    } else if (_places.length != 0) {
      double lat = _places[0].lat;
      double long = _places[0].long;
      return Stack(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(child: GoogleMapPage(lat, long, _places)),
            ],
          ),
          Positioned(
            bottom: 0.0,
            child: Container(
              height: 200.0,
              width: MediaQuery.of(context).size.width,
              child: PageView.builder(
                controller: _pageController,
                itemCount: _places.length,
                itemBuilder: (BuildContext context, int index) {
                  return AnimatedBuilder(
                    animation: _pageController,
                    builder: (BuildContext context, Widget widget) {
                      double value = 1;
                      if (_pageController.position.haveDimensions) {
                        value = _pageController.page - index;
                        value =
                            (1 - (value.abs() * 0.4) + 0.06).clamp(0.2, 1.0);
                      }
                      return Center(
                        child: SizedBox(
                          height: Curves.easeInOut.transform(value) * 175.0,
                          //width: Curves.easeInOut.transform(value) * 350.0,
                          child: Container(child: widget),
                        ),
                      );
                    },
                    child: InkWell(
                      onTap: () {
                        
                        http
                            .put(
                                "http://35.246.213.39:81/api/toilet/visit/${_places[index].id}")
                            .
                            // http.post("https://newdayshotel.com/demo/get.php",body:{
                            //     "name": "g$rating",
                            //   }).
                            then(
                          (value) {
                            if (value.statusCode == 200) {
                              print(value.body.toString());
                              //  Navigator.of(context).pop(false);
                              print(
                                  "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
                            } else {
                              print(value.statusCode.toString());
                            }
                          },
                        );
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) {
                          // return RouteMap();
                          return Direction(
                              lat,
                              long,
                              _places[index].distance,
                              _places[index].locaiton_lat,
                              _places[index].location_long,
                              _places[index]);
                        }));
                      },
                      child: Stack(
                        children: [
                          Center(
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                horizontal: 10.0,
                                vertical: 20.0,
                              ),
                              height: 120.0,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  // color: Colors.black,
                                  borderRadius: BorderRadius.circular(20.0),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black54,
                                      offset: Offset(0.0, 4.0),
                                      blurRadius: 10.0,
                                    ),
                                  ]),
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20.0),
                                    color: Colors.pink),
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Align(
                                        alignment: Alignment.bottomRight,
                                        child: Card(
                                          elevation: 5,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(70),
                                            ),
                                          ),
                                          child: Container(
                                            clipBehavior: Clip.antiAlias,
                                            height: 50,
                                            width: 50,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color:
                                                  Colors.blue.withOpacity(0.2),
                                            ),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: Center(
                                                child: FittedBox(
                                                  child: Text(
                                                    "${(_places[index].distance * 1000).toInt()}\nm",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        fontFamily: "Raleway",
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Row(children: [
                                      SizedBox(width: 10.0),
                                      Expanded(
                                        child: Container(
                                          padding: EdgeInsets.all(8),
                                          child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  child: Text(
                                                    //_places[index].name,
                                                    _places[index].name,
                                                   // maxLines: 1,

                                                    //softWrap: true,
                                                    // overflow:
                                                    //     TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontSize: 15.5,
                                                        color: Colors.white,
                                                        fontFamily: "Raleway",
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                Text(
                                                  _places[index].address,
                                                  maxLines: 1,
                                                  softWrap: true,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      fontSize: 15.0,
                                                      color: Colors.white,
                                                      fontFamily: "Raleway",
                                                      fontWeight:
                                                          FontWeight.normal),
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    Container(
                                                      // width: 170.0,
                                                      child: Text(
                                                        _places[index].rating!=null?"${_places[index].rating}":"",
                                                        style: TextStyle(
                                                            fontSize: 17.0,
                                                            color: Colors.white,
                                                            fontFamily:
                                                                "Raleway",
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ),
                                                    Icon(
                                                      Icons.star,
                                                      color: Colors.yellow,
                                                    )
                                                  ],
                                                )
                                              ]),
                                        ),
                                      )
                                    ]),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          notification
              ? Positioned(
                  left: MediaQuery.of(context).size.width * 0.1,
                  right: MediaQuery.of(context).size.width * 0.1,
                  top: MediaQuery.of(context).size.height * 0.1,
                  child: Padding(
                    padding: EdgeInsets.all(12),
                    child: Card(
                      child: Stack(children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.56,
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: 35),
                                child: 
                                CachedNetworkImage(
                                    height:
                                      MediaQuery.of(context).size.height * 0.48,
                                  width:
                                      MediaQuery.of(context).size.width * 0.8,
                                    placeholder: (context, url) =>
                                        SpinKitThreeBounce(
                                      color: Colors.white,
                                      size: 10,
                                    ),
                                    imageUrl:
                                        widget.url,
                                  ),
                                
                               
                              ),
                            ],
                          ),
                        ),
                        IconButton(
                            icon: Icon(
                              Icons.close,
                              color: Colors.red,
                              size: 30,
                            ),
                            onPressed: () {
                              setState(() {
                                notification = false;
                              });
                            }),
                      ]),
                    ),
                  ),
                )
              : Container(),
        ],
      );
    }
  }

  List<PlaceDetailModel> _places;
  PageController _pageController;
  BitmapDescriptor pinLocationIcon;

  @override
  void initState() {
    super.initState();

    if (_places == null) {
      UserLocation.get().getNearbyPlaces().then((data) {
        this.setState(() {
          _places = data;
          print(_places.toString());
        });
      });
    }

    _pageController = PageController(initialPage: 1, viewportFraction: 0.76);
  }
}
