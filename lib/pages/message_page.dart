import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:sanitation/pages/search.dart';
import 'package:http/http.dart' as http;

class MessagePage extends StatefulWidget {
  MessagePage({Key key}) : super(key: key);

  @override
  _MessagePageState createState() => _MessagePageState();
}

class _MessagePageState extends State<MessagePage> {
  List allAds;

  getAds() {
    http.get("http://35.246.213.39:81/api/ad/all",
        headers: {"Accept": "application/json"}).then((value) {
      setState(() {
        if (value.statusCode == 200) {
          allAds = json.decode(value.body)['message'];
        }
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getAds();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // backgroundColor: Color(0xFFcb0051),
        body: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(0.0)),
            gradient: LinearGradient(
              colors: <Color>[
                Color(0xFFff0066),
                // Color(0xFFcb0051),
                Color(0xFFff0066),
                // Color(0xFFdb0057),
              ],
            ),
          ),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 0),
            child: Stack(children: <Widget>[
              allAds!=null?Padding(
                padding: EdgeInsets.all(8),
                child: Padding(
                  padding:  EdgeInsets.only(left: 20),
                  child: CachedNetworkImage(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.contain,
                    placeholder: (context, url) => SpinKitThreeBounce(
                      color: Colors.white,
                      size: 10,
                    ),
                    imageUrl:
                        'http://35.246.213.39:81/storage/${allAds[4]['picture_url']}',
                  ),
                ),
              ):SpinKitThreeBounce(
                      color: Colors.white,
                      size: 10,
                    ),
              Align(
                alignment: Alignment.bottomCenter,
                child: AnimatedContainer(
                  duration: Duration(seconds: 2),
                  // Provide an optional curve to make the animation feel smoother.
                  curve: Curves.bounceIn,
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ButtonTheme(
                    minWidth: 80,
                    height: 30,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) {
                              return Search();
                            },
                          ),
                        );
                      },
                      padding: const EdgeInsets.all(0.0),
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                      child: Container(
                        width: 90,
                        height: 40,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.white, width: 2),
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          gradient: LinearGradient(
                            colors: <Color>[Colors.pink, Colors.pink],
                          ),
                        ),
                        padding: const EdgeInsets.all(0.0),
                        child: const Text(
                          'Next',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontFamily: "Raleway",
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
