import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:location/location.dart';
import 'package:sanitation/pages/direction.dart';
import 'package:sanitation/pages/map.dart';
import 'package:sanitation/pages/search.dart';
import 'package:url_launcher/url_launcher.dart';
import '../models/place_model.dart';
import '../services/gplace_service.dart';

class PlaceDetailPage extends StatefulWidget {
  var name;
  var index;
  PlaceDetailPage(this.name, this.index);

  @override
  State createState() => new PlaceDetailState();
}

class PlaceDetailState extends State<PlaceDetailPage> {
  String _currentPlaceId;
  bool showCircle = true;
  bool notification = true;
  void setNotification() {
    setState(() {
      notification = false;
      print("hello");
    });
  }

  static Future<LocationData> getLocation() async {
    var location = await Location().getLocation();
    return location;
  }

//   Future<String> getData() async{
//     var mylocation = await getLocation();
//     double lat = mylocation.latitude;
//     double long = mylocation.longitude;
// var reponse = await http.post("https://newdayshotel.com/demo/get.php",body:{
//         "name": "hello",
//       });

//       // List dataCafe = json.decode(reponse.body)["results"];
//       print(reponse.body.toString());
//   }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
        onWillPop: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) {
                return Search();
              },
            ),
          );
        },
        child: new Scaffold(body: _createContent()));
  }

  calculateDistance(double lat1, double lon1, double lat2, double lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    double a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;

    return (12742 * asin(sqrt(a)) * 1000);
  }

  // Future<double> locationDistance(double endLat, double endLong, double startlat,double startlong) async {
  //   //double distance = await _places[index].distance();
  //   //print(_places[index].distance());
  //   //print("from yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
  //   //return await Geolocator().distanceBetween(startlat, startlong, endLat ,endLong);
  // }
  // Future<double> dis;
  final _biggerFont =
      const TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600);
  Widget _createContent() {
    // getData();
    if (_places == null) {
      //  UserLocation.get().getPlace().then((data) {
      //   //getData();
      //   this.setState(() {
      //   _places = data;
      //   });
      // });
      return Stack(
        children: <Widget>[
          GoogleMapPage(9, 30),
          Container(
            color: Colors.transparent,
            child: Center(
                child: SpinKitPulse(
              color: Colors.blue.withOpacity(0.2),
              size: 500,
              duration: Duration(seconds: 2),
            )),
          ),
        ],
      );
    } else if (_places.length == 0) {
      //  UserLocation.get().getPlace().then((data) {
      //   //getData();
      //   this.setState(() {
      //     _places = data;
      //   });
      // });
      // getData();
      // LocationService.get(widget.name).getNearbyPlaces().then((data) {
      //   this.setState(() {
      //     _places = data;
      //   });
      // });
      //   LocationService.get(widget.name).getNearbyPlaces().then((data) {
      //     //getData();
      //     this.setState(() {
      //       _places = data;
      //     });
      //  });
      return Stack(
        children: <Widget>[
          GoogleMapPage(9, 30),
          Container(
            color: Colors.transparent,
            child: Center(
                child: SpinKitPulse(
              color: Colors.blue.withOpacity(0.2),
              size: 500,
              duration: Duration(seconds: 2),
            )),
          ),
        ],
      );
    } else if (_places.length != 0) {
      double lat = _places[0].lat;
      double long = _places[0].long;
      return Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              setState(() {
                showCircle = false;
              });
            },
            onTapDown: (TapDownDetails details) {
              setState(() {
                showCircle = false;
              });
            },
            child: Stack(
              children: <Widget>[
                Container(child: GoogleMapPage(lat, long, _places)),
              ],
            ),
          ),
          Positioned(
            bottom: 0.0,
            child: Container(
              height: 200.0,
              width: MediaQuery.of(context).size.width,
              child: PageView.builder(
                controller: _pageController,
                itemCount: _places.length,
                itemBuilder: (BuildContext context, int index) {
                  return AnimatedBuilder(
                    animation: _pageController,
                    builder: (BuildContext context, Widget widget) {
                      double value = 1;
                      if (_pageController.position.haveDimensions) {
                        value = _pageController.page - index;
                        value =
                            (1 - (value.abs() * 0.4) + 0.06).clamp(0.2, 1.0);
                      }
                      return Center(
                        child: SizedBox(
                          height: Curves.easeInOut.transform(value) * 175.0,
                          //width: Curves.easeInOut.transform(value) * 350.0,
                          child: Container(child: widget),
                        ),
                      );
                    },
                    child: InkWell(
                      onTap: () {
                        _launchMapsUrl(_places[index].name);
                      },
                      child: Stack(
                        children: [
                          Center(
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                horizontal: 10.0,
                                vertical: 20.0,
                              ),
                              height: 1235.0,
                              width: 400.0,
                              decoration: BoxDecoration(
                                  // color: Colors.black,
                                  borderRadius: BorderRadius.circular(20.0),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black54,
                                      offset: Offset(0.0, 4.0),
                                      blurRadius: 10.0,
                                    ),
                                  ]),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20.0),
                                  color: Colors.pink,
                                ),
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Align(
                                        alignment: Alignment.bottomRight,
                                        child: Card(
                                            elevation: 5,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(70))),
                                            child: Container(
                                                clipBehavior: Clip.antiAlias,
                                                height: 50,
                                                width: 50,
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: Colors.blue
                                                      .withOpacity(0.2),
                                                ),
                                                child: Center(
                                                  child: Text(
                                                    "${(calculateDistance(_places[index].locaiton_lat, _places[index].location_long, _places[index].lat, _places[index].long)).toInt()}\nm",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        fontFamily: "Raleway",
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ))),
                                      ),
                                    ),
                                    Row(children: [
                                      Container(
                                        height: 90.0,
                                        width: 20.0,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(10.0),
                                              topLeft: Radius.circular(10.0)),
                                        ),
                                        // child: Image.asset(
                                        //   "assets/seredvice${widget.index}.png",
                                        //   fit: BoxFit.contain,
                                        // ),
                                      ),
                                      SizedBox(width: 5.0),
                                      Expanded(
                                        child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                child: Text(
                                                  //_places[index].name,
                                                  _places[index].name,
                                                  maxLines: 1,

                                                  softWrap: true,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      fontSize: 16.5,
                                                      color: Colors.white,
                                                      fontFamily: "Raleway",
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                              Text(
                                                _places[index].vicinity,
                                                maxLines: 1,
                                                softWrap: true,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 15.0,
                                                    color: Colors.white,
                                                    fontFamily: "Raleway",
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Container(
                                                    // width: 170.0,
                                                    child: Text(
                                                      "3",
                                                      style: TextStyle(
                                                          fontSize: 17.0,
                                                          color: Colors.white,
                                                          fontFamily: "Raleway",
                                                          fontWeight:
                                                              FontWeight.w300),
                                                    ),
                                                  ),
                                                  Icon(
                                                    Icons.star,
                                                    color: Colors.yellow,
                                                  )
                                                ],
                                              )
                                            ]),
                                      )
                                    ]),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          // Align(
          //     alignment: Alignment.bottomCenter,
          //     child: Container(
          //       color: Colors.transparent,
          //       height: 150,
          //       child: new GridView(
          //         scrollDirection: Axis.horizontal,
          //         gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          //           crossAxisCount: 1,
          //           childAspectRatio: (130 / 200),
          //         ),
          //         children: _places.map((f) {
          //           int Distance = (calculateDistance(
          //                   f.locaiton_lat, f.location_long, f.lat, f.long))
          //               .toInt();
          //           return Container(
          //             //color: Colors.transparent,
          //             width: 200,
          //             child: GestureDetector(
          //               onTap: (){
          //                  Navigator.of(context).push(
          //                                   new MaterialPageRoute(builder:
          //                                       (BuildContext context) {
          //                                 return RouteMap(f.lat,f.long,f.locaiton_lat,f.location_long);
          //                               }));
          //               },
          //                                     child: new Card(

          //                 clipBehavior: Clip.antiAlias,
          //                 elevation: 10,
          //                 shape: RoundedRectangleBorder(
          //                     borderRadius:
          //                         BorderRadius.all(Radius.circular(20))),
          //                 child: Stack(
          //                   // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //                   children: <Widget>[
          //                     Padding(
          //                       padding: const EdgeInsets.all(8.0),
          //                       child: Align(
          //                         alignment: Alignment.bottomRight,
          //                         child: Card(
          //                             elevation: 5,
          //                             shape: RoundedRectangleBorder(
          //                                 borderRadius: BorderRadius.all(
          //                                     Radius.circular(70))),
          //                             child: Container(
          //                                 clipBehavior: Clip.antiAlias,
          //                                 height: 50,
          //                                 width: 50,
          //                                 decoration: BoxDecoration(

          //                                   shape: BoxShape.circle,
          //                                   color: Colors.blue.withOpacity(0.2),
          //                                 ),
          //                                 child: Center(
          //                                   child: Text(
          //                                     "$Distance.0\nm",
          //                                     textAlign: TextAlign.center,
          //                                     style: TextStyle(fontSize: 14,fontWeight: FontWeight.w500),
          //                                   ),
          //                                 ))),
          //                       ),
          //                     ),
          //                     Padding(
          //                       padding: const EdgeInsets.all(0.0),
          //                       child: Align(
          //                         alignment: Alignment.bottomLeft,
          //                         child: Image.asset("assets/b.png", height: 100,

          //                             //"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CmRaAAAAP70MTuomvj-9mMAGWbzUd2oHqN2wxHZx1OKmLCj63UOf_On92ycWmoQKcgT88hu1NZ43EalmQS-pMKiDiPdPdZKnW9WsBaRJP_zF1SVTy5-3OeuoDMWFsjrPO28Ad7ZqEhANvB_7m2TeKApyMfxPDv1BGhRS2fZNA7Fqqo0sGp_YPKdXa3ABGA&key=AIzaSyAbiU3lrWQpjMmHKB9lkCsXiSo1rkRqahA",
          //                             fit: BoxFit.cover),
          //                       ),
          //                     ),
          //                     Container(
          //                       height: 60,
          //                       width: double.infinity,
          //                       decoration: BoxDecoration(
          //                         boxShadow: [
          //                           BoxShadow(
          //                             color: Colors.purple.withOpacity(0.3),
          //                           )
          //                         ],
          //                         color: Colors.black12,
          //                         borderRadius: (BorderRadius.only(
          //                             bottomLeft: Radius.circular(50),
          //                             bottomRight: Radius.circular(0))),
          //                       ),
          //                       child: Column(
          //                         children: <Widget>[
          //                           FittedBox(
          //                               child: Padding(
          //                             padding: const EdgeInsets.symmetric(
          //                                 horizontal: 14),
          //                             child: Text(f.name, style: _biggerFont,),
          //                           )),
          //                           FittedBox(
          //                             child: Padding(
          //                               padding: const EdgeInsets.all(8.0),
          //                               child: Text(f.vicinity),
          //                               // this is not the
          //                             ),
          //                           ),
          //                         ],
          //                       ),
          //                     ),
          //                   ],
          //                 ),
          //               ),
          //             ),
          //           );
          //         }).toList(),
          //       ),
          //     )),
         notification
                              ? Positioned(
                                  left: MediaQuery.of(context).size.width * 0.1,
                                  right:
                                      MediaQuery.of(context).size.width * 0.1,
                                  top: MediaQuery.of(context).size.height * 0.1,
                                  child: Padding(
                                    padding: EdgeInsets.all(12),
                                    child: Card(
                                      child: Stack(children: <Widget>[
                                        Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.56,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.8,
                                          child: Column(
                                            children: <Widget>[
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(top: 35),
                                                child: Image.asset(
                                                  "assets/corona.jpg",
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.48,
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.8,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        IconButton(
                                            icon: Icon(
                                              Icons.close,
                                              color: Colors.red,
                                              size: 30,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                notification = false;
                                              });
                                            }),
                                      ]),
                                    ),
                                  ),
                                )
                              : Container(),
        ],
      );
    }
  }

  void _launchMapsUrl(String destinationPlaceName) async {
    final url =
        'https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination=$destinationPlaceName';

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  List<PlaceDetail> _places;

  PageController _pageController;
  void _onScroll() {
    // if (_pageController.page.toInt() != prevPage) {
    //   prevPage = _pageController.page.toInt();
    //   moveCamera();
    // }
    // BitmapDescriptor.fromAssetImage(
    //         ImageConfiguration(devicePixelRatio: 2.5), 'assets/toilet.png')
    //     .then((onValue) {
    //   pinLocationIcon = onValue;
    // });
  }

//   Future<String> getData() async{
//     var mylocation = await getLocation();
//     double lat = mylocation.latitude;
//     double long = mylocation.longitude;
// var reponse = await http.post("https://newdayshotel.com/demo/get.php",body:{
//         "name": "hello",
//       });

//       // List dataCafe = json.decode(reponse.body)["results"];
//       print(reponse.body.toString());
//   }
  @override
  void initState() {
    super.initState();
    //dis = locationDistance(0);
    //getData();

    if (_places == null || _places.length == 0) {
      LocationService.get(widget.name).getNearbyPlaces().then((data) {
        this.setState(() {
          _places = data;
        });
      });
    }

    _pageController = PageController(initialPage: 1, viewportFraction: 0.7);

    // print("count: "+_places.length.toString());
  }

  VoidCallback onItemTapped;
}
